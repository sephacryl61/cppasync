/* Copyright 2016 by Tay Paik Kang

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met :

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and / or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or
promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. */

#ifndef _CPPASYNC_H_
#define _CPPASYNC_H_

#include <functional>
#include <future>
#include <memory>

/*!
    Base class for Task Items, based on command pattern
*/
class AsyncCommand
{
public:
    virtual void execute() = 0;
    virtual bool check() const = 0;

    static void queue(std::unique_ptr<AsyncCommand> cmd);
    static bool executeOnce();
    static void executeToCompletion();
};

template<class T,class E> struct WhenPrivate;

/*!
    Task with single return value and error handling
*/
template<class T,class E>
class Task : public AsyncCommand
{
    std::function<T()> opfn_;
    std::function<void(T &&)> tdfn_;
    std::function<void(E)> erfn_;
    mutable std::future<T> result_;
    friend struct WhenPrivate<T,E>;

public:
    Task(std::function<T()> &&opfn) : opfn_(opfn), tdfn_([](T&&){}), erfn_([](E){}) {}
    Task(Task &&r) : opfn_(std::move(r.opfn_)), tdfn_(std::move(r.tdfn_)), erfn_(std::move(r.erfn_)) {}
    void execute()
    {
        std::packaged_task<T()> pkgtask(std::move(opfn_));
        result_ = pkgtask.get_future();
        pkgtask();
    }
    bool check() const
    {
        if (result_.valid()) {
            try {
                tdfn_(result_.get());
            }
            catch (E err) {
                erfn_(err);
            }
            return true;
        }
        else return false;
    }
};

template<class E>
class Task<void,E> : public AsyncCommand
{
    std::function<void()> opfn_;
    std::function<void()> tdfn_;
    std::function<void(E)> erfn_;
    mutable std::future<void> result_;
    friend struct WhenPrivate<void,E>;

public:
    Task(std::function<void()> &&opfn) : opfn_(opfn), tdfn_([] {}), erfn_([](E) {}) {}
    Task(Task &&r) : opfn_(std::move(r.opfn_)), tdfn_(std::move(r.tdfn_)), erfn_(std::move(r.erfn_)) {}
    void execute()
    {
        std::packaged_task<void()> pkgtask(std::move(opfn_));
        result_ = pkgtask.get_future();
        pkgtask();
    }
    bool check() const
    {
        if (result_.valid()) {
            try {
                result_.get();
                tdfn_();
            }
            catch (E err) {
                erfn_(err);
            }
            return true;
        }
        else return false;
    }
};

template<class T,class E>
struct WhenPrivate
{
    std::unique_ptr<Task<T,E> > task_;
    WhenPrivate(std::function<T()> &&opfn) : task_(std::unique_ptr<Task<T,E> >(new Task<T,E>(std::move(opfn)))) {}
    WhenPrivate(WhenPrivate &&r) : task_(std::move(r.task_)) {}
    ~WhenPrivate()
    {
        if (task_) AsyncCommand::queue(std::move(task_));
    }
    WhenPrivate then(std::function<void(T &&)> &&tdfn)
    {
        task_->tdfn_ = std::move(tdfn);
        return WhenPrivate(std::move(*this));
    }
    void otherwise(std::function<void(E)> &&erfn)
    {
        task_->erfn_ = std::move(erfn);
    }
};

template<class E>
struct WhenPrivate<void,E>
{
    std::unique_ptr<Task<void,E> > task_;
    WhenPrivate(std::function<void()> &&opfn) : task_(std::unique_ptr<Task<void,E> >(new Task<void,E>(std::move(opfn)))) {}
    WhenPrivate(WhenPrivate &&r) : task_(std::move(r.task_)) {}
    ~WhenPrivate()
    {
        if (task_) AsyncCommand::queue(std::move(task_));
    }
    WhenPrivate then(std::function<void()> &&tdfn)
    {
        task_->tdfn_ = std::move(tdfn);
        return WhenPrivate(std::move(*this));
    }
    void otherwise(std::function<void(E)> &&erfn)
    {
        task_->erfn_ = std::move(erfn);
    }
};

template<class T,class E=int>
WhenPrivate<T,E> when(std::function<T()> &&opfn)
{
    return WhenPrivate<T,E>(std::move(opfn));
}

#endif /* _CPPASYNC_H_ */
