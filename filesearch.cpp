/* Copyright 2016 by Tay Paik Kang

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met :

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and / or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or
promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. */

#include <iostream>
#include <chrono>
#include <cstdio>
#include <algorithm>
#include "cppasync.h"

struct search_result
{
    size_t found, total;
};

int main(int argc, char *argv[])
{
    auto const start = std::chrono::steady_clock::now();
    auto files_done = 0;
    const int files_total = argc - 1;
    search_result report = { 0, 0 };
    for (int idx = 1; idx < argc; ++idx)
    {
        when<search_result>([=]
        {
            search_result result = { 0, 0 };
            std::FILE *fd = std::fopen(argv[idx], "r");
            if (fd == nullptr) throw 2;
            while (!std::feof(fd))
            {
                char buffer[1024];
                auto const sz = fread(buffer, sizeof(char), sizeof(buffer), fd);
                if (sz == 0 && std::ferror(fd) > 0) {
                        std::fclose(fd); throw 2;
                }
                result.found += std::count_if(&buffer[0], &buffer[sz], [&](const char &ch) {
                    if ((ch < ' ' || ch > '~') && !(ch == '\xa' || ch == '\xd' || ch == '\x9')) {
                        std::fclose(fd); throw 1;
                    }
                    return ch == 'e';
                });
                result.total += sz;
            }
            std::fclose(fd);
            return result;
        })
        .then([&](search_result &&result)
        {
            report.found += result.found;
            report.total += result.total;
            ++files_done;
        })
        .otherwise([=,&files_done](int err)
        {
            if (err == 1)      std::cout << argv[idx] << " skipped (binary file)\n";
            else if (err == 2) std::cout << argv[idx] << " cannot be opened\n";
            ++files_done;
        });
    }
    auto last = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start);
    std::cout << files_total << " async tasks scheduled in " << last.count() << "msec\n";
    do
    {
        auto const now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start);
        if (now - last >= std::chrono::milliseconds(1000)) {
            last = now;
            std::cout << now.count() << "msec elapsed - " << files_done << " of " << files_total << " files processed (" << files_done * 100 / files_total << "%)\n";
        }
    } while (AsyncCommand::executeOnce());
    std::cout << "Found " << report.found << ", Searched " << report.total << ", Time taken " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() << "ms" << std::endl;

    return 0;
}
