/* Copyright 2016 by Tay Paik Kang

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met :

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
the following disclaimer in the documentation and / or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or
promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. */

#include "cppasync.h"
#include <iostream>
#include <string>
#include <thread>
#include <random>
#include <chrono>

int main()
{
    std::string str1 = "Hello ";
    const char str2[] = "How ";
    const char str3[] = "are you?\n";
    bool err = false;

    when<void>([] {
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(0, 15);
        auto const timeout = distribution(generator);
        if (timeout < 3) throw 0;
        std::this_thread::sleep_for(std::chrono::seconds(timeout));
    })
    .then([] {
        std::cout << "Woke up from sleep\n";
    }).otherwise([](int) {
        std::cout << "Nightmare!\n";
    });

    when<std::string>([&]
    {
        return str1;
    })
    .then([&](std::string &&str1)
    {
        std::cout << str1 << str2;
        err = true;
    })
    .otherwise([&](int)
    {
        std::cout << str3;
    });

    when<std::string>([&]
    {
        return str1;
    })
    .then([&](std::string &&str1)
    {
        if (err) throw 0;
        std::cout << str1 << str2;
    })
    .otherwise([&](int)
    {
        std::cout << str3;
    });

    for (int idx = 0; idx < 200; ++idx)
    {
        when<int>([=]
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            return idx;
        })
        .then([](int v)
        {
            std::cout << v + 20 << ' ';
        });
    }

    for (int idx=0; AsyncCommand::executeOnce(); ++idx)
        if (idx % 1000 == 0)
        {
            std::cout << ".";
        }
    return 0;
}

