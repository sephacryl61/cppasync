# Async Library for C++ #

The async library makes use of the features of C++11 to
provide a simple interface for functions that block, or
take a long time to complete so that they can be executed
asynchronously, and the results integrated back to the
main thread.

```c++
#include "cppasync.h"
#include <iostream>
#include <random>
#include <thread>

int main()
{
    when<void>([]{
        /* == Async function == */
        std::default_random_engine generator;
        std::uniform_int_distribution<int> distribution(0, 15);
        auto timeout = distribution(generator);
        if (timeout < 3) throw 0;
        std::this_thread::sleep_for(std::chrono::seconds(timeout));
    })
    .then([]{
        /* == Fulfillment == */
        std::cout << "Woke up refreshed\n";
    })
    .otherwise([](int){
        /* == Rejection == */
        std::cout << "Nightmare!\n";
    });

    for (int idx=0; AsyncCommand::executeOnce(); ++idx)
        if (idx % 1000 == 0) std::cout << ".";
    return 0;
}
```

The library works with any **Callable** target (function, lambda
expression, bind expression, or another function object). It will
return immediately after scheduling the async task for completion
by another thread. Async tasks are transferred using **command 
pattern**, to be serviced from a thread pool.

The async function throws an int exception if there is any error,
which would be transferred to the main thread and handled by the
rejection function.

Fulfillment and rejection functions are optional. If present,
they will be run during *AsyncCommand::executeOnce*, in the main
thread. You should be able to schedule async tasks anywhere in
your main thread, including inside fulfillment or rejection functions.

Typically, the async function will produce result that is
transferred to the fulfillment function. This is passed in the template
argument for *when*. Since, the return value of async is a temporary,
the fulfillment function takes a rvalue reference of the specialized
type.

For more examples of usage, see [simpletest](/sephacryl61/cppasync/blob/master/simpletest.cpp) and [filesearch](/sephacryl61/cppasync/blob/master/filesearch.cpp).

## Installing a C++11 compiler and Building the Example ##

### Windows ###
Visual Studio 2015

```command
qmake -t vcapp -spec win32-msvc2015 -o simpletest.vcproj simpletest.pro
```

### Linux ###
gcc 4.8.1, or clang 3.4

#### Installing gcc 4.8.1 in Ubuntu 12.04 ####
Ubuntu 12.04 ships with 4.6 and you need to install 4.8.1 manually.

```bash
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install gcc-4.8
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 50

qmake simpletest.pro
```

#### Installing clang 3.4 in Ubuntu 12.04 ####
Ubuntu 12.04 ships with 3.0 and you need to install 3.4 manually.

```bash
sudo apt-get update
sudo apt-get install clang-3.4

qmake -spec unsupported/linux-clang simpletest.pro
```