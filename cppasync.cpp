/* Copyright 2016 by Tay Paik Kang

Redistribution and use in source and binary forms, with or without modification, are permitted provided
that the following conditions are met :

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the
   following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
   the following disclaimer in the documentation and / or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. */

#include "cppasync.h"
#include <list>
#include <mutex>
#include <thread>
#include <algorithm>

const int NUM_THREADS = 8;
const int WAIT_CYCLES = 10;
static std::list<std::unique_ptr<AsyncCommand> > asynccmdlist, donelist;
static std::mutex asynccmdlistmutex, donelistmutex;
static std::thread threadpool[NUM_THREADS];

void AsyncCommand::queue(std::unique_ptr<AsyncCommand> cmd)
{
    // 1a. Look for a thread to run this command immediately
    for (int idx = 0; idx < NUM_THREADS; ++idx)
    {
        if (!threadpool[idx].joinable())
        {
            // 2. Thead at idx is free to run
            {
                std::lock_guard<std::mutex> unused(asynccmdlistmutex);
                if (!asynccmdlist.empty()) {
                    // 3. If pending task add cmd to end and run oldest pending task instead
                    cmd.swap(asynccmdlist.front());
                    asynccmdlist.splice(asynccmdlist.end(), asynccmdlist, asynccmdlist.begin());
                }
            }

            auto const cmdtmpptr = cmd.release();
            threadpool[idx] = std::thread([=] {
                std::list<std::unique_ptr<AsyncCommand> > cmdlst(1);
                cmdlst.front().reset(cmdtmpptr);
                do {
                    // 4. Execute
                    cmdlst.front()->execute();

                    // 5. Move to donelist
                    {
                        std::lock_guard<std::mutex> unused(donelistmutex);
                        donelist.splice(donelist.end(), cmdlst, cmdlst.begin());
                    }

                    for (int cnt = 0; cnt < WAIT_CYCLES || !threadpool[idx].joinable(); ++cnt)
                    {
                        std::unique_lock<std::mutex> asynccmdlistlock(asynccmdlistmutex);
                        if (asynccmdlist.empty()) {
                            // 6a. No commands left, wait and see
                            asynccmdlistlock.unlock();
                            std::this_thread::yield();
                        }
                        else {
                            // 6b. Move command to current working list
                            cmdlst.splice(cmdlst.end(), asynccmdlist, asynccmdlist.begin());
                            break;
                        }
                    }
                } while (!cmdlst.empty());
                threadpool[idx].detach();
            });
            return;
        }
    }
    // 1b. No avialable thread, add to pending task
    {
        std::lock_guard<std::mutex> unused(asynccmdlistmutex);
        asynccmdlist.push_back(std::move(cmd));
    }
}

bool AsyncCommand::executeOnce()
{
    bool donelistempty;
    {
        std::lock_guard<std::mutex> unused(donelistmutex);
        donelistempty = donelist.empty();
        donelist.remove_if([](const std::unique_ptr<AsyncCommand> &cmd) { return cmd->check(); });
    }
    return !donelistempty || (std::count_if(&threadpool[0], &threadpool[NUM_THREADS], [](const std::thread &th) {return !th.joinable(); })) != NUM_THREADS;
}

void AsyncCommand::executeToCompletion()
{
    while (executeOnce());
}
